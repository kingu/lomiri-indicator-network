Service		com.lomiri.connectivity1
Interface	com.lomiri.connectivity1.NetworkingStatus
Object path	/com/lomiri/connectivity1/NetworkingStatus
Policy group	connectivity
Supports	org.freedesktop.DBus.Introspectable
		org.freedesktop.DBus.Properties

Properties	array{string} Limitations [readonly]

			String array representing the possible limitations
			on networking.

			Currently available limitations are:
				"bandwidth"

			"bandwidth" - indicates that the bandwidth of the
				Internet connection has limitations.
				Applications should minimize their bandwidth
				usage if possible.

		string Status [readonly]

			Status of the Internet connectivity. 

			Possible values:
				"offline", "connecting", "online"
