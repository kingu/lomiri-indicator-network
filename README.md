# Lomiri Indicator Network

Lomiri Indicator Network is the network indicator for the Lomiri
operating shell used in Ubuntu Touch.

## Introduction

Lomiri's indicator-network service is responsible for exporting the network
settings menu through D-Bus.

This indicator is basically an Ayatana system indicator, but it has been
specifically been designed for the Lomiri Operating Environment. If you
try using it with another desktop environment, note that you are on
unexplored territory.

## i18n: Translating Lomiri's Indicator Network into your Language

You can easily contribute to the localization of this project (i.e. the
translation into your language) by visiting (and signing up with) the
Hosted Weblate service:
https://hosted.weblate.org/projects/lomiri/lomiri-indicator-network

The localization platform of this project is sponsored by Hosted Weblate
via their free hosting plan for Libre and Open Source Projects.
